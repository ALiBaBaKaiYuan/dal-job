//package com.kvn.dal.core.multi_node;
//
//import java.util.Random;
//
//import org.joda.time.DateTime;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//import org.springframework.stereotype.Service;
//
//import com.kvn.dal.core.TimedTask;
//import com.kvn.dal.core.task.ExecutableTask;
//
///**
// * Created by wzy on 2017/5/24.
// */
//@TimedTask(corn = "0/15 * * * * ?", isGlobalSingle = false, desc = "测试MultiNodeJob")
//@Service
//public class MultiNodeJob implements ExecutableTask {
//    @Override
//    public void execute(JobExecutionContext context) throws JobExecutionException {
//        System.out.println(DateTime.now() + "--" + Thread.currentThread().getName() + "---------------doBizJob--------");
//        try {
//            Thread.sleep(2000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        if(new Random().nextInt() % 2 == 0){
//        	throw new RuntimeException("biz执行异常，xxxxxxxxx");
//        }
//    }
//}
