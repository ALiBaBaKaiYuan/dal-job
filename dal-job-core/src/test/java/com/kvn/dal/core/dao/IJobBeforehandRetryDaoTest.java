package com.kvn.dal.core.dao;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kvn.dal.core.pojo.enums.JobBeforehandRetryStatus;

/**
* @author wzy
* @date 2017年7月18日 上午9:33:58
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/application.xml")
public class IJobBeforehandRetryDaoTest {
	@Resource
	IJobBeforehandRetryDao jobBeforehandRetryDao;

	@Test
	public void testUpdateStatus() {
		jobBeforehandRetryDao.updateStatus(13L, JobBeforehandRetryStatus.BIZ_FAIL, JobBeforehandRetryStatus.BIZ_INIT_TEMP, "业务失败[end]", "xxx");
	}

	@Test
	public void testUpdate() {
		jobBeforehandRetryDao.myupdate("RETRY_INIT", 14L);
	}
}
