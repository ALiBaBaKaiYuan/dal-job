package com.kvn.dal.core.afterward_retry;
/**
* @author wzy
* @date 2017年6月23日 下午6:01:57
*/
public class Foo {
	private int id;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Foo(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Foo() {
		super();
	}
	

}
