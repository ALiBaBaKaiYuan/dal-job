package com.kvn.dal.core.beforehand_retry;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kvn.dal.core.afterward_retry.Foo;

/**
* @author wzy
* @date 2017年6月23日 下午7:26:03
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/application.xml")
public class BeforehandRetryTest {
	@Resource
	BeforehandRetryBizService retryBiz;

	@Test
	public void testExecuteBiz() {
		try {
			retryBiz.doBiz(new Foo(1002, "beforeRetryXxx"), "hehe");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			TimeUnit.MINUTES.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 事前重试，重试方法不含参数
	 */
	@Test
	public void testDoBizWithoutParam() {
		try {
			retryBiz.doBizWithoutParam();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			TimeUnit.MINUTES.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
