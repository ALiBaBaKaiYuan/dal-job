package com.kvn.dal.core.afterward_retry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.kvn.dal.core.dao.IJobRetryDao;
import com.kvn.dal.core.pojo.JobRetry;
import com.kvn.dal.core.retry.RetryParam;
import com.kvn.dal.core.retry.RetryParamListWrapper;
import com.kvn.dal.core.retry.afterward.AfterwardRetryContext;
import com.kvn.dal.core.retry.afterward.IRetrySupport;

/**
 * Created by wzy on 2017/5/24.
 */
@Service
public class AfterwardRetryBizService implements IRetrySupport {
    @Resource
    IJobRetryDao jobRetryDao;

    public void executeBiz() {
        System.out.println(DateTime.now() + "--" + Thread.currentThread().getName() + "---------------doBizJob2222--------");
        try {
            Thread.sleep(3000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Foo foo = new Foo(1001, "xxx");

        try {
            throw new RuntimeException("executeBiz异常，xxxxxxxxx");
        } catch (Exception e) {
            // 记录异常，后面重试
            RetryParam param1 = RetryParam.createParam(foo);
            RetryParam param2 = RetryParam.createParam("hehehe");
            RetryParam param3 = RetryParam.createParam("morning");
            JobRetry retry = JobRetry.createJobRetry(this.getClass(), "key001", param1, param2, param3);
            
            /********** 简易API *********/
//            ArrayList<RetryParam> retryLs = new RetryParamListWrapper().buildRetryParam(foo).buildRetryParam("xxx").buildRetryParam("hehehe").toArrayList();
//            JobRetry retry2 = JobRetry.createJobRetry(this.getClass(), "key001", retryLs);
            
            jobRetryDao.add(retry);

            throw e;
        }
    }

    /**
     * <pre>
     * 输出结果如下：
     * ----->retryDataKey:key001
     * ------------------通过retryContext.getRetryParamLs()去取--------------
     * ---->originFoo:{"id":1001,"name":"xxx"}
     * ---->originParam:hehehe
     * ---->originParam2:morning
     * ------------------通过retryContext.getRetryParamLs()去取--------------
     * ---->originParam:hehehe
     * ---->originParam:morning
     * ---->originFoo:{"id":1001,"name":"xxx"}
     * </pre>
     */
    @Override
    public Boolean retry(AfterwardRetryContext retryContext) {
        String retryDataKey = retryContext.getRetryDataKey();
        List<RetryParam> paramLs = retryContext.getRetryParamLs();
        Foo foo = paramLs.get(0).retoreParam(Foo.class);
        String originParam = paramLs.get(1).retoreParam(String.class);
        String originParam2 = paramLs.get(2).retoreParam(String.class);
        System.out.println("----->retryDataKey:" + retryDataKey);
        System.out.println("------------------通过retryContext.getRetryParamLs()去取--------------");
        System.out.println("---->originFoo:" + JSON.toJSONString(foo));
        System.out.println("---->originParam:" + originParam);
        System.out.println("---->originParam2:" + originParam2);
        System.out.println("------------------通过retryContext.getRetryParamLs()去取--------------");
        System.out.println("---->originParam:" + retryContext.getRetryParamValueMap().get(String.class).get(0));
        System.out.println("---->originParam:" + retryContext.getRetryParamValueMap().get(String.class).get(1));
        System.out.println("---->originFoo:" + JSON.toJSONString(retryContext.getRetryParamValueMap().get(Foo.class).get(0)));
        System.out.println("=====>" + retryContext.getRetryParamValueMap());

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return true;
    }
    
    public static void main(String[] args) {
    	Foo foo = new Foo(1001, "xxx");
    	ArrayList<RetryParam> al2 = new RetryParamListWrapper().buildRetryParam(foo).buildRetryParam("xxx").buildRetryParam("hehehe").toArrayList();
        System.out.println(JSON.toJSONString(al2));
        System.out.println(JSON.toJSONString(al2).equals(JSON.toJSONString(al2)));
	}
}
