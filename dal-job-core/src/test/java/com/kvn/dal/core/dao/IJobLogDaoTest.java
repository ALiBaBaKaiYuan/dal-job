package com.kvn.dal.core.dao;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kvn.dal.core.pojo.enums.JobStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/application.xml")
public class IJobLogDaoTest {
	@Resource
	IJobLogDao jobLogDao;

	@Test
	public void testUpdate() {
		jobLogDao.update(256, JobStatus.SUCCESS, JobStatus.FAIL, "123");
	}

}
