package com.kvn.dal.core.afterward_retry;

import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
* @author wzy
* @date 2017年6月23日 下午7:26:03
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/application.xml")
public class RetryBizTest {
	@Resource
	AfterwardRetryBizService retryBiz;
	@Resource
	AfterwardRetryBestPracticeService afterwardRetryBestPracticeService;
	@Resource
	AfterwardRetryBestPracticeService2 afterwardRetryBestPracticeService2;

	/**
	 * 通过实现IRetrySupport接口的方式，来重试<br/>
	 * 重试方法包含入参
	 */
	@Test
	public void testExecuteBiz() {
		try {
			retryBiz.executeBiz();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			TimeUnit.MINUTES.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 通过继承AbstractRetrySupport类的方式，来重试。<br/>
	 * 重试方法包含入参
	 */
	@Test
	public void testAfterwardRetryBestPracticeService() {
		try {
			afterwardRetryBestPracticeService.executeBiz();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			TimeUnit.MINUTES.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 通过继承AbstractRetrySupport类的方式，来重试。<br/>
	 * 重试方法没有入参
	 */
	@Test
	public void testAfterwardRetryBestPracticeService2() {
		try {
			afterwardRetryBestPracticeService2.executeBiz();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			TimeUnit.MINUTES.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	

}
