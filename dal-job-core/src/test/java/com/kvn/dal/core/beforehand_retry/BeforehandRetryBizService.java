package com.kvn.dal.core.beforehand_retry;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.kvn.dal.core.afterward_retry.Foo;
import com.kvn.dal.core.exception.BizRetryNeedException;
import com.kvn.dal.core.retry.beforehand.BeforehandRetry;
import com.kvn.dal.core.retry.beforehand.ThreadContext;

/**
 * @author wzy
 * @date 2017年7月14日 下午5:27:56
 */
@Service
public class BeforehandRetryBizService {
	
	@BeforehandRetry
	public String doBiz(Foo foo, String param){
		System.out.println("--->isRetryThread:" + ThreadContext.getContext().isRetryThread());
		System.out.println("参数：Foo=" + JSON.toJSONString(foo) + ", param=" + param);
		System.out.println("执行业务失败>>>>>>>>");
		throw new BizRetryNeedException("业务失败，需要重试！！！");
	}
	
	@BeforehandRetry
	public String doBizWithoutParam(){
		System.out.println("--->isRetryThread:" + ThreadContext.getContext().isRetryThread());
		System.out.println("doBizWithoutParam#执行业务失败>>>>>>>>");
		throw new BizRetryNeedException("业务失败，需要重试！！！");
	}

}
