package com.kvn.dal.core.afterward_retry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.kvn.dal.core.retry.RetryParam;
import com.kvn.dal.core.retry.RetryParamListWrapper;
import com.kvn.dal.core.retry.afterward.AbstractRetrySupport;
import com.kvn.dal.core.retry.afterward.AfterwardRetryContext;

/**
 * 重试方法没有入参
 * Created by wzy on 2017/5/24.
 */
@Service
public class AfterwardRetryBestPracticeService2 extends AbstractRetrySupport {

	public void executeBiz() {
		System.out.println(DateTime.now() + "--" + Thread.currentThread().getName() + "---------------doBizJob2222--------");
		try {
			Thread.sleep(3000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try {
			throw new RuntimeException("executeBiz异常，xxxxxxxxx");
		} catch (Exception e) {
			/** 重试方法没有入参  **/
			this.retryEnqueue(null);
			throw e; // 出异常后，终止业务
		}
	}

	/**
	 * <pre>
	 * 输出结果如下：
	 * </pre>
	 */
	@Override
	public Boolean retry(AfterwardRetryContext retryContext) {
		String retryDataKey = retryContext.getRetryDataKey();
		List<RetryParam> paramLs = retryContext.getRetryParamLs();
		System.out.println("----->retryDataKey:" + retryDataKey);
		System.out.println("----->RetryParamLs:" + paramLs);

		try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

	public static void main(String[] args) {
		Foo foo = new Foo(1001, "xxx");
		ArrayList<RetryParam> al2 = new RetryParamListWrapper().buildRetryParam(foo).buildRetryParam("xxx").buildRetryParam("hehehe").toArrayList();
		System.out.println(JSON.toJSONString(al2));
		System.out.println(JSON.toJSONString(al2).equals(JSON.toJSONString(al2)));
	}
}
