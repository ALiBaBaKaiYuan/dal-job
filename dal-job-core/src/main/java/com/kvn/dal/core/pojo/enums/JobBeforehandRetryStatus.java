package com.kvn.dal.core.pojo.enums;

import org.quartz.JobExecutionContext;

/**
 * 事前重试状态：biz_init|biz_success|biz_fail|retry_init|retry_success|retry_fail
 * @author wzy
 * @date 2017年6月23日 下午8:14:42
 */
public enum JobBeforehandRetryStatus {
	BIZ_INIT, // 业务执行前的初始化状态
	/**
	 * 业务超时重试时使用
	 * @see com.kvn.dal.core.retry.beforehand.RetryBeforehandDispatchJob#execute(JobExecutionContext)
	 */
	BIZ_INIT_TEMP, // 业务初始临时状态，不入DB。
	BIZ_SUCCESS, // 业务执行成功
	BIZ_FAIL, // 业务执行失败（不会重试）
	RETRY_INIT, // 重试初始状态（重试起点状态）
	RETRY_SUCCESS, // 重试执行业务成功
	RETRY_FAIL; // 重试失败（可以继续重试）
	
	public static void main(String[] args) {
		System.out.println(BIZ_INIT.toString());
		System.out.println(BIZ_INIT.equals("BIZ_INIT"));
	}
}
