package com.kvn.dal.core.exception;

import com.kvn.dal.exception.ApplicationException;

/**
 * @author wzy
 * @date 2017年7月14日 下午5:16:38
 */
public class BizRetryNeedException extends ApplicationException {

	private static final long serialVersionUID = -3673995892990405055L;

	public BizRetryNeedException(int code, String message) {
		super(code, message);
	}

	public BizRetryNeedException(int code, String message, Throwable cause) {
		super(code, message, cause);
	}

	public BizRetryNeedException(String message) {
		super(message);
	}

	public BizRetryNeedException(String message, Throwable cause) {
		super(message, cause);
	}
}
