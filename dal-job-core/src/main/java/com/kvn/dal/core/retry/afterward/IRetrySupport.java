package com.kvn.dal.core.retry.afterward;

/**
* @author wzy
* @date 2017年6月23日 下午5:17:48
*/
public interface IRetrySupport {
	/**
	 * 重试
	 * @param retryContext 重试上下文
	 * @return 返回重试结果：true | false
	 */
	Boolean retry(AfterwardRetryContext retryContext);
}
