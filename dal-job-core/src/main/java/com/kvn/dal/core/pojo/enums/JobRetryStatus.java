package com.kvn.dal.core.pojo.enums;

/**
 * 重试状态
 * @author wzy
 * @date 2017年6月23日 下午8:14:42
 */
public enum JobRetryStatus {
	INIT, // 初始化状态 
	SUCCESS, // 重试成功
	FAIL, // 重试失败（正常失败）
	BUG_FAIL; // 重试程序抛出异常，有bug，终止重试
}
