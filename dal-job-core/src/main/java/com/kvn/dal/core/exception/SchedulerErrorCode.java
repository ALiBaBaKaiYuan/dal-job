package com.kvn.dal.core.exception;

import com.kvn.dal.exception.IErrors;

/**
 * Created by wzy on 2017/5/27.
 */
public enum SchedulerErrorCode implements IErrors {
    JOB_HAS_RUN(1, "job已经执行过"),
    MISS_TIMEDTASK_ANNOTATION(2, "[%s]缺失@TimedTask注解"),
    JOB_RUNING(3, "job正在其他机执行");


    private int code;
    private String desc;

    private SchedulerErrorCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public SchedulerException exp() {
        return new SchedulerException(code, desc);
    }

    @Override
    public SchedulerException exp(Object... args) {
        String formatReason = String.format(desc, args);
        return new SchedulerException(code, formatReason);
    }

    @Override
    public SchedulerException expMsg(String message, Object... args) {
        String formatReason = String.format(message, args);
        return new SchedulerException(code, formatReason);
    }

    @Override
    public SchedulerException exp(Throwable cause, Object... args) {
        String formatReason = String.format(desc, args);
        return new SchedulerException(code, formatReason, cause);
    }

    @Override
    public SchedulerException expMsg(String message, Throwable cause,
                               Object... args) {
        String formatReason = String.format(message, args);
        return new SchedulerException(code, formatReason, cause);
    }
}
