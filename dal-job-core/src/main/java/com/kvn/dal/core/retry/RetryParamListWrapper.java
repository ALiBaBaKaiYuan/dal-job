package com.kvn.dal.core.retry;

import java.util.ArrayList;

import com.google.common.collect.Lists;

/**
 * 为构造多个RetryParam提供更简易的API
 * @author wzy
 * @date 2017年7月18日 下午5:18:19
 */
public class RetryParamListWrapper {
	private ArrayList<RetryParam> retryParamLs;

	public RetryParamListWrapper() {
		super();
		retryParamLs = Lists.newArrayList();
	}

	public RetryParamListWrapper buildRetryParam(Object param) {
		RetryParam retryParam = RetryParam.createParam(param);
		this.retryParamLs.add(retryParam);
		return this;
	}

	public ArrayList<RetryParam> toArrayList() {
		return this.retryParamLs;
	}

}
