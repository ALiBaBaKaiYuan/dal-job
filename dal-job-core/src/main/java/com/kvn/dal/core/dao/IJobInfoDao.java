package com.kvn.dal.core.dao;

import org.apache.ibatis.annotations.Param;

import com.kvn.dal.core.pojo.JobInfo;

public interface IJobInfoDao {

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table job_info
     *
     * @mbggenerated
     */
    int add(JobInfo record);

    JobInfo selectOne(String jobCode);

    JobInfo select4update(String jobCode);

    int optimisticLock(@Param("id") Long id, @Param("version") long version);
}