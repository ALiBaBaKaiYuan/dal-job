package com.kvn.dal.core.pojo;

import java.util.Date;

import com.kvn.dal.core.pojo.enums.JobStatus;
import com.kvn.dal.core.task.ExecutableTask;

/**
 * Created by wzy on 2017/5/24.
 */
public class JobLog {
    private long id;
    private String code;
    private String cron;
    private boolean globalSingle; // 是否只在单台机执行
    private String description;
    private Date startTime;
    private Date finishTime;
    private JobStatus jobStatus;
    private String executeType; // system, manual
    private int count; // 执行次数
    private String remark;

    private transient ExecutableTask task;
    private transient Class<? extends ExecutableTask> executableTaskClass;

    public boolean isGlobalSingle() {
		return globalSingle;
	}

	public void setGlobalSingle(boolean globalSingle) {
		this.globalSingle = globalSingle;
	}

	public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Class<? extends ExecutableTask> getExecutableTaskClass() {
		return executableTaskClass;
	}

	public void setExecutableTaskClass(Class<? extends ExecutableTask> executableTaskClass) {
		this.executableTaskClass = executableTaskClass;
	}

	public ExecutableTask getTask() {
        return task;
    }

    public void setTask(ExecutableTask task) {
        this.task = task;
    }

    public String getCode() {
        return code;
    }

    public String getExecuteType() {
        return executeType;
    }

    public void setExecuteType(String executeType) {
        this.executeType = executeType;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public JobLog bindJobInfo(JobLog jobLog) {
        this.setId(jobLog.getId());
        this.setCode(jobLog.getCode());
        this.setCron(jobLog.getCron());
        this.setDescription(jobLog.getDescription());
        this.setStartTime(jobLog.getStartTime());
        this.setJobStatus(jobLog.getJobStatus());
        this.setExecuteType(jobLog.getExecuteType());
        this.setCount(jobLog.getCount());
        this.setRemark(jobLog.getRemark());
        return this;
    }

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}
}
