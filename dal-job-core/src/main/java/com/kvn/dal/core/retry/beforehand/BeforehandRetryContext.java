package com.kvn.dal.core.retry.beforehand;

import com.kvn.dal.core.pojo.JobBeforehandRetry;
import com.kvn.dal.core.retry.AbstractRetryContext;

/**
 * 事前重试上下文
 * 
 * @author wzy
 * @date 2017年7月14日 下午6:14:28
 */
public class BeforehandRetryContext extends AbstractRetryContext<JobBeforehandRetry> {

	private JobBeforehandRetry jobRetryRecord;

	@Override
	public JobBeforehandRetry getJobRetryRecord() {
		return jobRetryRecord;
	}

	public void setJobRetryRecord(JobBeforehandRetry jobRetryRecord) {
		this.jobRetryRecord = jobRetryRecord;
	}

}
