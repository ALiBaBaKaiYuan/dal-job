package com.kvn.dal.core.pojo;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.kvn.dal.core.pojo.enums.JobRetryStatus;
import com.kvn.dal.core.retry.RetryParam;
import com.kvn.dal.core.retry.afterward.IRetrySupport;

public class JobRetry {
    /**
     * pk
     */
    private Long id;

    /**
     * 重试class的全名称
     */
    private String retryClassName;

    /**
     * 重试时，取数的key
     */
    private String retryDataKey;
    
    /**
     * 需要重试的次数
     */
    private Integer maxRetryCount;
    /** 默认的最大重试次数 */
    public static final Integer MAX_RETRYCOUNT_DEFAULT = 3;

    /**
     * 重试次数
     */
    private Integer retryCount;

    /**
     * 重试状态
     * @see JobRetryStatus
     */
    private JobRetryStatus status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 重试参数集
     */
    private String retryParams;
    
    public static JobRetry createJobRetry(Class<? extends IRetrySupport> retryClass, String retryDataKey, RetryParam... params){
    	// 默认重试3次
    	return createJobRetry(retryClass, retryDataKey, JobRetry.MAX_RETRYCOUNT_DEFAULT, params);
    }
    
    public static JobRetry createJobRetry(Class<? extends IRetrySupport> retryClass, String retryDataKey, ArrayList<RetryParam> paramLs){
    	// 默认重试3次
    	return createJobRetry(retryClass, retryDataKey, JobRetry.MAX_RETRYCOUNT_DEFAULT, paramLs);
    }
    
    public static JobRetry createJobRetry(Class<?> retryClass, String retryDataKey, int maxRetryCount, RetryParam... params){
    	ArrayList<RetryParam> retryParamLs = params == null ? null : Lists.newArrayList(params);
    	return createJobRetry(retryClass, retryDataKey, maxRetryCount, retryParamLs);
    }
    
    public static JobRetry createJobRetry(Class<?> retryClass, String retryDataKey, int maxRetryCount, ArrayList<RetryParam> paramLs){
    	JobRetry jobRetry = new JobRetry();
    	jobRetry.retryClassName = retryClass.getName();
    	jobRetry.retryDataKey = retryDataKey == null ? "NULL" : retryDataKey; // 重试方法没有入参时，可以不设置retryDataKey
    	
    	jobRetry.setMaxRetryCount(maxRetryCount);
    	if(!CollectionUtils.isEmpty(paramLs)){
    		jobRetry.retryParams = JSON.toJSONString(Lists.newArrayList(paramLs.toArray()));
    	}
    	
    	return jobRetry;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRetryClassName() {
        return retryClassName;
    }

    public void setRetryClassName(String retryClassName) {
        this.retryClassName = retryClassName == null ? null : retryClassName.trim();
    }

    public String getRetryDataKey() {
        return retryDataKey;
    }

    public void setRetryDataKey(String retryDataKey) {
        this.retryDataKey = retryDataKey == null ? null : retryDataKey.trim();
    }

	public Integer getMaxRetryCount() {
		return maxRetryCount;
	}

	public void setMaxRetryCount(Integer maxRetryCount) {
		this.maxRetryCount = maxRetryCount;
	}

	public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public JobRetryStatus getStatus() {
        return status;
    }

    public void setStatus(JobRetryStatus status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRetryParams() {
        return retryParams;
    }

    public void setRetryParams(String retryParams) {
        this.retryParams = retryParams == null ? null : retryParams.trim();
    }
}