package com.kvn.dal.core.retry;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 重试参数
* @author wzy
* @date 2017年6月23日 下午5:21:12
*/
public class RetryParam {
	
	/**
	 * 重试参数的类型
	 */
	private Class<?> paramClass;
	
	/**
	 * 重试参数的值。使用JSON保存
	 */
	private String paramValue;

	public Class<?> getParamClass() {
		return paramClass;
	}

	public void setParamClass(Class<?> paramClass) {
		this.paramClass = paramClass;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	
	/**
	 * 创建一个RetryParam
	 * @param paramClass
	 * @param paramValue
	 * @return
	 */
	public static <T> RetryParam createParam(Class<T> paramClass, T paramValue){
		RetryParam param = new RetryParam();
		param.paramClass = paramClass;
		param.paramValue = JSON.toJSONString(paramValue);
		return param;
	}
	
	/**
	 * 创建一个RetryParam
	 * @param paramValue
	 * @return
	 */
	public static RetryParam createParam(Object paramValue){
		RetryParam param = new RetryParam();
		param.paramClass = paramValue.getClass();
		param.paramValue = JSON.toJSONString(paramValue);
		return param;
	}
	
	/**
	 * 恢复重试参数
	 * @return
	 */
	public <T> T retoreParam(Class<T> paramClass){
		return JSONObject.parseObject(paramValue, paramClass);
	}
	
//	public static ArrayListWrapper<RetryParam> build(){
//		return ArrayListWrapper.create();
//	}
	
}
