package com.kvn.dal.core.retry.beforehand;

/**
 * 线程的线程上下文
* @author wzy
* @date 2017年7月17日 下午3:12:07
*/
public class ThreadContext {
	private static final ThreadLocal<ThreadContext> LOCAL = new ThreadLocal<ThreadContext>() {
		@Override
		protected ThreadContext initialValue() {
			return new ThreadContext();
		}
	};
	
	/**
	 * get context.
	 * 
	 * @return context
	 */
	public static ThreadContext getContext() {
	    return LOCAL.get();
	}
	
	private boolean retryThread;

	public boolean isRetryThread() {
		return retryThread;
	}

	public void setRetryThread(boolean retryThread) {
		this.retryThread = retryThread;
	}
	
	/**
	 * clear thread-local variable
	 */
	public static void clear(){
		LOCAL.remove();
	}

}
