package com.kvn.dal.core.retry;

import java.util.List;

import com.google.common.collect.ListMultimap;

/**
 * 重试的上下文
* @author wzy
* @date 2017年6月23日 下午5:15:53
*/
public abstract class AbstractRetryContext<T> {
	
	
	/**
	 * 重试参数集
	 */
	private List<RetryParam> retryParamLs;
	
	/**
	 * 重试参数Map<参数类型，参数值>
	 */
	private ListMultimap<Class<?>, Object> retryParamValueMap;
	
	/**
	 * 获取重试记录
	 * @return
	 */
	public abstract T getJobRetryRecord();

	public List<RetryParam> getRetryParamLs() {
		return retryParamLs;
	}

	public void setRetryParamLs(List<RetryParam> retryParamLs) {
		this.retryParamLs = retryParamLs;
	}

	public ListMultimap<Class<?>, Object> getRetryParamValueMap() {
		return retryParamValueMap;
	}

	public void setRetryParamValueMap(ListMultimap<Class<?>, Object> retryParamValueMap) {
		this.retryParamValueMap = retryParamValueMap;
	}

}
