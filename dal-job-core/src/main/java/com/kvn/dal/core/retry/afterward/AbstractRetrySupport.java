package com.kvn.dal.core.retry.afterward;

import java.util.ArrayList;

import javax.annotation.Resource;

import com.google.common.collect.Lists;
import com.kvn.dal.core.dao.IJobRetryDao;
import com.kvn.dal.core.pojo.JobRetry;
import com.kvn.dal.core.retry.RetryParam;

/**
 * 提供重试工具函数
 * 
 * @author wzy
 * @date 2017年7月19日 上午10:12:13
 */
public abstract class AbstractRetrySupport implements IRetrySupport {
	@Resource
	private IJobRetryDao jobRetryDao;

	/**
	 * 重试记录入队，落到DB
	 * 
	 * @param retryDataKey
	 *            获取重试数据的key，可以为空
	 * @param args
	 *            重试参数
	 */
	protected void retryEnqueue(String retryDataKey, Object... retryArgs) {
		// 默认重试3次
		this.retryEnqueue(retryDataKey, JobRetry.MAX_RETRYCOUNT_DEFAULT, retryArgs);
	}

	/**
	 * 重试记录入队，落到DB
	 * 
	 * @param retryDataKey
	 *            获取重试数据的key，可以为空
	 * @param args
	 *            重试参数
	 * @param maxRetryCount
	 *            最大重试次数
	 */
	protected void retryEnqueue(String retryDataKey, int maxRetryCount, Object... retryArgs) {
		if (retryArgs == null) { // 认为重试方法没有入参
			JobRetry retry = JobRetry.createJobRetry(this.getClass(), retryDataKey, maxRetryCount);
			jobRetryDao.add(retry);
			return;
		}

		ArrayList<RetryParam> retryLs = Lists.newArrayList();
		for (Object arg : retryArgs) {
			RetryParam param = RetryParam.createParam(arg);
			retryLs.add(param);
		}
		JobRetry retry = JobRetry.createJobRetry(this.getClass(), retryDataKey, maxRetryCount, retryLs);
		jobRetryDao.add(retry);
	}
}
