package com.kvn.dal.core.pojo;

import java.util.Date;

public class JobInfo {
    /**
     * 主键
     */
    private Long id;

    /**
     * 任务编码
     */
    private String code;

    /**
     * cron表达式
     */
    private String cron;
    
    /**
     *  是否只在单台机执行
     */
    private boolean globalSingle; 

    /**
     * 任务描述
     */
    private String description;

    /**
     * 版本号（乐观锁）
     */
    private long version; // 乐观锁

    /**
     * 创建时间
     */
    private Date createTime;

    
    public boolean isGlobalSingle() {
		return globalSingle;
	}

	public void setGlobalSingle(boolean globalSingle) {
		this.globalSingle = globalSingle;
	}

	public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}
}