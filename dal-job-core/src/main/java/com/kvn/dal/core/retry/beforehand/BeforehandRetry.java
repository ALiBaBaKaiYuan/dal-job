package com.kvn.dal.core.retry.beforehand;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.kvn.dal.core.exception.BizRetryNeedException;

/**
 * 事前补偿，确保每次执行业务时都有留底。会牺牲一性的性能。
 * @author wzy
 * @date 2017年7月14日 下午5:03:45
 */
@Target({ METHOD })
@Retention(RUNTIME)
@Inherited
public @interface BeforehandRetry {
	/**
	 * 执行重试的异常，默认是对BizRetryNeedException才去执行重试逻辑。业务异常是不需要重试的！！！
	 * @return
	 */
	Class<? extends Throwable>[] retryFor() default BizRetryNeedException.class;
	
	/**
	 * 最大重试次数
	 * @return
	 */
	int maxRetryCount() default 3;
}
