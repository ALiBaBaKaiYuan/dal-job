package com.kvn.dal.core;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by wzy on 2017/5/24.
 */
@Target({ TYPE })
@Retention(RUNTIME)
public @interface TimedTask {
    String cron(); // 支持spring properties文件，通过${key}来取值
    boolean isGlobalSingle() default true; // 分布式环境下，是否单台启动
    String desc() default "";
}
