package com.kvn.dal.core.exception;

import com.kvn.dal.exception.ApplicationException;

/**
 * Created by wzy on 2017/5/27.
 */
public class SchedulerException extends ApplicationException {
	private static final long serialVersionUID = -5521027876231891108L;

	public SchedulerException(int code, String message) {
        super(code, message);
    }

    public SchedulerException(int code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public SchedulerException(String message) {
        super(message);
    }

    public SchedulerException(String message, Throwable cause) {
        super(message, cause);
    }
}
