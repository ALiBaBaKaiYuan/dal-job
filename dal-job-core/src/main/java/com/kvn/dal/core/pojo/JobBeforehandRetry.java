package com.kvn.dal.core.pojo;

import java.util.Date;

import com.kvn.dal.core.pojo.enums.JobBeforehandRetryStatus;

public class JobBeforehandRetry {
    /**
     * pk
     */
    private Long id;

    /**
     * 重试class的全名称
     */
    private String retryClassName;
    
    /**
     * 需要重试的方法名称
     */
    private String retryMethod;

    /**
     * 最大retry的次数
     */
    private Integer maxRetryCount;

    /**
     * 重试次数
     */
    private Integer retryCount;

    /**
     * 状态：biz_init|biz_success|biz_fail|retry_init|retry_success|retry_fail
     */
    private JobBeforehandRetryStatus status;
    
    /**
     * 伪状态：要使用biz_init_temp时使用。不需要持久化
     */
//    private transient JobBeforehandRetryStatus maskStatus;
    
    /**
     * 流程路线
     */
    private String flowPath;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 重试参数集。默认使用JSON序列化
     */
    private String retryParams;

    public String getRetryMethod() {
		return retryMethod;
	}

	public void setRetryMethod(String retryMethod) {
		this.retryMethod = retryMethod;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRetryClassName() {
        return retryClassName;
    }

    public void setRetryClassName(String retryClassName) {
        this.retryClassName = retryClassName == null ? null : retryClassName.trim();
    }

    public Integer getMaxRetryCount() {
        return maxRetryCount;
    }

    public void setMaxRetryCount(Integer maxRetryCount) {
        this.maxRetryCount = maxRetryCount;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public JobBeforehandRetryStatus getStatus() {
    	return status;
    }
    
    public void setStatus(JobBeforehandRetryStatus status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRetryParams() {
        return retryParams;
    }

    public void setRetryParams(String retryParams) {
        this.retryParams = retryParams == null ? null : retryParams.trim();
    }

	public String getFlowPath() {
		return flowPath;
	}

	public void setFlowPath(String flowPath) {
		this.flowPath = flowPath;
	}

//	public JobBeforehandRetryStatus getMaskStatus() {
//		return maskStatus;
//	}
//
//	public void setMaskStatus(JobBeforehandRetryStatus maskStatus) {
//		this.maskStatus = maskStatus;
//	}
}