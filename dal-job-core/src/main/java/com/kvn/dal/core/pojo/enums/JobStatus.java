package com.kvn.dal.core.pojo.enums;

/**
 * Created by wzy on 2017/5/24.
 */
public enum JobStatus {
    PROCESSING,
    SUCCESS,
    FAIL;
}
