package com.kvn.dal.core.task;

import org.quartz.Job;

/**
 * Created by wzy on 2017/5/24.
 */
public interface ExecutableTask extends Job {
}
