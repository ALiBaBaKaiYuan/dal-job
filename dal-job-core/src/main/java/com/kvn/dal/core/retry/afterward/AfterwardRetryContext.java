package com.kvn.dal.core.retry.afterward;

import com.kvn.dal.core.pojo.JobRetry;
import com.kvn.dal.core.retry.AbstractRetryContext;

/**
* @author wzy
* @date 2017年7月14日 下午6:11:13
*/
public class AfterwardRetryContext extends AbstractRetryContext<JobRetry> {
	/**
	 * 重试时，取数的key
	 */
	private String retryDataKey;
	
	/**
	 * 重试记录
	 */
	private JobRetry jobRetryRecord;

	@Override
	public JobRetry getJobRetryRecord() {
		return jobRetryRecord;
	}

	public void setJobRetryRecord(JobRetry jobRetryRecord) {
		this.jobRetryRecord = jobRetryRecord;
	}
	
	public String getRetryDataKey() {
		return retryDataKey;
	}

	public void setRetryDataKey(String retryDataKey) {
		this.retryDataKey = retryDataKey;
	}
}
