package com.kvn.dal.common;

import org.springframework.context.EmbeddedValueResolverAware;
import org.springframework.util.StringValueResolver;

/**
* @author wzy
* @date 2017年7月18日 下午1:02:33
*/
public class PropertyValueResolverUtil implements EmbeddedValueResolverAware {
	private StringValueResolver stringValueResolver;

	/**
	 * 解析${key}
	 * @param pattern
	 * @return
	 */
	public String resolve(String pattern){
		return stringValueResolver.resolveStringValue(pattern);
	}
	
	@Override
	public void setEmbeddedValueResolver(StringValueResolver resolver) {
		this.stringValueResolver = resolver;
	}

}
