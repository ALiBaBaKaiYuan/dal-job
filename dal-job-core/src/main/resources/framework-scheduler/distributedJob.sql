CREATE TABLE `job_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(100) NOT NULL COMMENT '任务编码',
  `cron` varchar(30) NOT NULL COMMENT 'cron表达式',
  `description` varchar(100) DEFAULT NULL COMMENT '任务描述',
  `global_single` varchar(10) NOT NULL COMMENT '在集群下是否单台执行（true|false）',
  `version` bigint(20) NOT NULL DEFAULT '0' COMMENT '版本号（乐观锁）',
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_job_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='job信息表';


CREATE TABLE `job_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(100) NOT NULL COMMENT '任务编码',
  `cron` varchar(30) NOT NULL COMMENT 'cron表达式',
  `description` varchar(100) DEFAULT NULL COMMENT '任务描述',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `finish_time` datetime DEFAULT NULL COMMENT '结束时间',
  `job_status` varchar(20) NOT NULL COMMENT '任务状态(PROCESSING|SUCCESS|FAIL)',
  `execute_type` varchar(20) NOT NULL COMMENT '执行类型：system|manual',
  `global_single` varchar(10) NOT NULL COMMENT '在集群下是否单台执行（true|false）',
  `count` int(11) NOT NULL COMMENT '执行次数',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COMMENT='job日志表';


CREATE TABLE `job_retry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'pk',
  `retry_class_name` varchar(255) NOT NULL COMMENT '重试class的全名称',
  `retry_data_key` varchar(50) NOT NULL COMMENT '重试时，取数的key',
  `retry_params` text COMMENT '重试参数集',
  `max_retry_count` int(11) NOT NULL COMMENT '最大retry的次数',
  `retry_count` int(11) NOT NULL COMMENT '重试次数',
  `status` varchar(50) NOT NULL COMMENT '重试状态：init|success|fail',
  `flow_path` varchar(200) NOT NULL COMMENT '流程路线',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='重试记录表';


CREATE TABLE `job_beforehand_retry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'pk',
  `retry_class_name` varchar(255) NOT NULL COMMENT '重试class的全名称',
  `retry_method` varchar(50) NOT NULL COMMENT '重试方法名称',
  `retry_params` text COMMENT '重试参数集',
  `max_retry_count` int(11) NOT NULL COMMENT '最大retry的次数',
  `retry_count` int(11) NOT NULL COMMENT '重试次数',
  `status` varchar(50) NOT NULL COMMENT '状态：biz_init|biz_success|biz_fail|retry_init|retry_success|retry_fail',
  `flow_path` varchar(200) NOT NULL COMMENT '流程路线',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='事前重试记录表';